#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>
#include <string.h>


struct Joueurs
{
  int   socket;
  char  pseudo[1024];
  int   score;
  int   connect;
};  



void concat_to_buffer(char *bufferOrigin, char *c)
{
  strcat (bufferOrigin,c);
}


char* display_menu()
{
  return "\n\n###############################################\n               BIENVENUE AU QUIZ GAME          \n###############################################\n\n Choisissez un pseudo : ";
}

// initialise un tableau de questions -- evolution : parse fichier 
void init_tab_question(char **question)
{

  question[0] = "Combien de doigts on chacune des 2 mains";
  question[1] = "zéro";
  question[2] = "cinq";
  question[3] = "dix";
  question[4] = "vingt";
  question[5] = "2";
  question[6] = "Qui est l'inventeur du langage C";
  question[7] = "Donald Knuth";
  question[8] = "Alan Turing";
  question[9] = "Denis Ritchie";
  question[10] = "Steve Jobs";
  question[11] = "3";
  question[6] = "Quand est mort J.S Bach";
  question[7] = "-100 av JC";
  question[8] = "1600";
  question[9] = "1700";
  question[10] = "1750";
  question[11] = "3";
}

// retourne un id de question aléatoire
int get_id_question()
{
  return (rand() % 2)*6;
}

// retourne une question de l'id
char* get_question(int id ,char **question)
{
  char *rep;
  
  rep=malloc(100*sizeof(char));
  strcat(rep,question[id]);
  strcat(rep, "\n 1- ");
  strcat(rep, question[id+1]);
  strcat(rep, "\n 2- ");
  strcat(rep, question[id+2]);
  strcat(rep, "\n 3- ");
  strcat(rep, question[id+3]);
  strcat(rep, "\n 4- ");
  strcat(rep, question[id+4]);  
  strcat(rep, "\n \n \n");

  return ("%s",rep);
}

// retourne la reponse écrite de la question
char* get_reponse(int q, char **question)
{
  int rep = (int) strtol(question[q+5], NULL, 10);
  return question[q+rep];
}

// retourne l'entier correspondant à la reponse de la question courante
char* get_indice_reponse(int q, char **question)
{
  return question[q+5];
}


void send_all_connect(int taille,struct Joueurs *connect, char *buffer)
{
  int k;
  // puis on l'envoie aux joueurs... 
  for(k=1;k<taille;k++)
    {
    // seulement aux joueurs connectés
    if(connect[k].connect == 1)
      if(send(connect[k].socket,buffer,1024,0) == -1)
	{              
	  perror("Erreur lors de l'appel a send -> ");
	  exit(1);
	}
    }
}


void display_score(int taille,struct Joueurs *connect, char *buffer)
{
  int k;
  strcpy(buffer,"");
  // faire la string des scores
  for(k=1;k<taille;k++)
    {
      // seulement aux joueurs connectés
      if(connect[k].connect == 1)
	{
	  strcat(buffer,connect[k].pseudo);
	  strcat(buffer," score = ");
          //printf("%s",connect[k].score);
	  strcat(buffer,"100");
	  strcat(buffer," \n");
	}
    }
  
  for(k=1;k<taille;k++)
    {
    // seulement aux joueurs connectés
      if(connect[k].connect == 1)
	if(send(connect[k].socket,buffer,1024,0) == -1)
	  {              
	    perror("Erreur lors de l'appel a send -> ");
	    exit(1);
	}
    }
}

int main(int argc, char **argv)
{
  srand(time(NULL));
  int socket_descriptor, socket_descriptor2,nb_connect=0;          // descripteurs de socket
  fd_set readfds;               // ensemble des descripteurs en lecture qui seront surveilles par select
  struct Joueurs joueur;
  struct Joueurs connect[FD_SETSIZE]; // tableau qui contiendra des structtures de joueurs logger, avec une taille egale a la taille max de l'ensemble d'une structure fd_set 
  int taille=0;                 // nombre de descripteurs dans le tableau precedent
  int en_attente=0;
  char buffer[1024];               // espace necessaire pour stocker le message recu
  char* menu,id_question;
  char* question[100];
  memset(buffer,'\0',1024);        // initialisation du buffer qui sera utilisé
  

  struct sockaddr_in my_addr;   // structure d'adresse qui contiendra les param reseaux du recepteur
  struct sockaddr_in client;    // structure d'adresse qui contiendra les param reseaux de l'expediteur

  // taille d'une structure sockaddr_in utile pour la fonction recvfrom
  socklen_t sin_size = sizeof(struct sockaddr_in);



  // verification du nombre d'arguments sur la ligne de commande
  if(argc != 2)
    {
      printf("Usage: %s port_local\n", argv[0]);
      exit(-1);
    }

  // creation de la socket
  socket_descriptor = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);

  // initialisation de la structure d'adresse du serveur (pg local)

  // famille d'adresse
  my_addr.sin_family = AF_INET;

  // recuperation du port du serveur
  my_addr.sin_port = ntohs(atoi(argv[1]));

  // adresse IPv4 du serveur
  my_addr.sin_addr.s_addr=htonl(INADDR_ANY);

  // association de la socket et des param reseaux du serveur
  if(bind(socket_descriptor,(struct sockaddr*)&my_addr,sizeof(my_addr)) != 0)
    {
      perror("Erreur lors de l'appel a bind -> ");
      exit(1);
    }

  // indication de la limite MAX de la file d'attente des connexions entrantes
  if(listen(socket_descriptor,10) != 0)
    {
      perror("Erreur lors de l'appel a listen -> ");
      exit(2);
    }


  // le premier descripteur est celui du serveur
  printf("Attente de connexion\n");
  joueur.socket = socket_descriptor;
  strcpy(joueur.pseudo,"SERVEUR");
  connect[0]=joueur;    // on ajoute deja la socket d'ecoute au tableau de descripteurs
  taille++;       // et donc on augmente "taille"

  // initialise le tableau de questions-reponses
  init_tab_question(question);
  
  while(1){               
    FD_ZERO(&readfds);           //il faut remettre tt les elements ds readfds a chaque recommencement de la boucle, vu que select modifie les ensembles
    int j;
    int sockmax=0;
    for(j=0;j<taille;j++){
      if(connect[j].socket != 0)              
        FD_SET(connect[j].socket,&readfds);  // on remet donc tous les elements dans readfds
      if(sockmax < connect[j].socket)                 // et on prend ici le "numero" de socket maximal pour la fonction select
        sockmax = connect[j].socket;
    }
                                 
    if(select(sockmax+1,&readfds,NULL,NULL,NULL) == -1){      // on utilise le select sur toutes les sockets y compris celle d'ecoute 
      perror("Erreur lors de l'appel a select -> ");
      exit(1);
    }
                                          
    if(FD_ISSET(socket_descriptor,&readfds)){    // si la socket d'ecoute est dans readfds, alors qqch lui a ete envoye (=connection d'un client)
      // on accepte la connexion entrante et on cree une socket...
      if((socket_descriptor2 = accept(socket_descriptor,(struct sockaddr*)&client,&sin_size)) == -1){ 
        perror("Erreur lors de accept -> ");
        exit(3);
      }
      printf("Connexion etablie avec %s\n", inet_ntoa(client.sin_addr));
      send(socket_descriptor2,display_menu(),1024,0);
      // ...qui est donc ajoutee au tableau de descripteurs
      // init d'un joueur
      joueur.score = 0;
      joueur.connect = 0;
      joueur.socket=socket_descriptor2;
      connect[taille]=joueur;
      taille++;
      if(taille>3)
	en_attente=1;
      else
	en_attente=0;
    }

    /******************************/
    /* RECUPERATION DE LA REPONSE */
    /******************************/
    int i;
    for(i=1;i<taille;i++){     // puis on l'envoie a tous les clients... 
      // si une socket du tableau est dans readfds, alors qqch a ete envoye au serveur par un client
      if(FD_ISSET(connect[i].socket,&readfds)){
	// on stocke alors le message
        if(recv(connect[i].socket,&buffer,1024,0) == -1){                   
          perror("Erreur lors de la reception -> ");
          exit(4);
        }

        printf("La chaine recue de %s est: %s\n",inet_ntoa(client.sin_addr),buffer);                                  // et on l'affiche
	
	//si non connecter alors le message est forcement son pseudo
        // si premiere connexion, souhaite la bienvenue
	if(connect[i].connect == 0){
	  snprintf(connect[i].pseudo, 1024, "%s", buffer);
	  connect[i].connect = 1;
	  nb_connect++;
	  concat_to_buffer(buffer," vient de se connecté \n");
	  send_all_connect(taille,connect,buffer);
	}//end if premiere connexion
	
	 // si connecté alors le buffer contient une reponse
	else {
	  if(get_indice_reponse(id_question,question)[0] == buffer[0])
	    {

	      // edite le buffer avec le gagnant
	      concat_to_buffer(strcpy(buffer,connect[i].pseudo)," a remporté la manche : +100 \n");
	      // attribution des point
	      connect[i].score+=100;
	      // envois a tout les connectés
	      send_all_connect(taille,connect,buffer);

	      concat_to_buffer(strcpy(buffer,"")," la bonne reponse était :");
	      concat_to_buffer(buffer, get_reponse((int) id_question,question));
	      send_all_connect(taille,connect,buffer);
	     
	    }
	  else
	    {
	      // edite le buffer avec le gagnant
	      concat_to_buffer(strcpy(buffer,connect[i].pseudo)," a mal répondu, il perd : -50 \n");
	      // attribution des point
	      connect[i].score-=50;
	      // envois a tout les connectés
	      send_all_connect(taille,connect,buffer);

	      concat_to_buffer(strcpy(buffer,"")," la bonne reponse était :");
	      concat_to_buffer(buffer, get_reponse((int) id_question,question));
	      send_all_connect(taille,connect,buffer);
	    }

	  en_attente=0;
	  sleep(5);
	}


	/************************/
	/* ENVOI DE LA QUESTION */
	/************************/
		
	//envoyer la question + reponses si 2 joueur.connect = 1
	if(nb_connect >= 2 && en_attente != 1){
	  concat_to_buffer(strcpy(buffer,""),"\n\n\n ========= SCORES ========");
	  send_all_connect(taille,connect,buffer);
	  display_score(taille,connect,buffer);
	  concat_to_buffer(strcpy(buffer,""),"\n =================");
	  send_all_connect(taille,connect,buffer);
	  // tirer l'id d'une question aléatoire
	  id_question = get_id_question();
	  snprintf(buffer, 1024, "%s", get_question(id_question,question));
	  send_all_connect(taille,connect,buffer);
	}
      }
    }
  }// end while          
}

